"use strict";

$(document).ready(function () {
  let overlay = $("#overlay");
  let closeBtn = $("#close-btn");
  let toggler = $("#toggler");

  toggler.click(function () {
    overlay.toggleClass("show-menu");
    $(this).fadeOut();
  });

  closeBtn.click(function () {
    overlay.toggleClass("show-menu");
    toggler.fadeIn();
  });

  $(".owl-carousel").owlCarousel({
    loop: true,
    margin: 0,
    responsiveClass: true,
    dots: false,

    responsive: {
      0: {
        items: 1,
        nav: true,
      },
      767.8: {
        items: 1,
        center: true,
        margin: 0,
        nav: true,
        autoWidth: true,
      },
      768: {
        items: 2,
        center: true,
        margin: 0,
        nav: true,
      },
      992: {
        margin: 25,
        items: 3,
        nav: true,
        loop: false,
        autoWidth: true,
      },
      1200: {
        margin: 25,
        items: 2,
        nav: true,
        loop: false,
        autoWidth: true,
      },
    },
  });
});
